package guiUAS;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.ScrollPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class FormBuku {

	JFrame frame;
	private JTextField textJudulBuku;
	private JTextField textPenerbit;
	private JTextField textPengarang;
	private JTextField textTahunTerbit;
	private ArrayList<Integer> textIdBuku;
	private JComboBox comboBoxStatus;
	private JTable table;
	private JButton btnSimpan;
	
	private DefaultTableModel model;
	
	// koneksi java-mysql
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://127.0.0.1/sewabuku";
	static final String USER = "root";
	static final String PASS = "";
			
	static Connection conn;
	static Statement stmt;
	static ResultSet rs;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormBuku window = new FormBuku();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FormBuku() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				showData();
			}
		});
		frame.setTitle("Perpustakaan Nakula");
		frame.setBounds(100, 100, 640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Data Buku");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(47, 22, 114, 25);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Judul Buku");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(47, 68, 70, 13);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Pengarang");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(47, 98, 70, 13);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Penerbit");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(47, 128, 70, 13);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Tahun Terbit");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_4.setBounds(47, 158, 70, 13);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Status");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_5.setBounds(47, 188, 70, 13);
		frame.getContentPane().add(lblNewLabel_5);
		
		textJudulBuku = new JTextField();
		textJudulBuku.setBounds(153, 64, 160, 24);
		frame.getContentPane().add(textJudulBuku);
		textJudulBuku.setColumns(10);
		
		textPenerbit = new JTextField();
		textPenerbit.setBounds(153, 94, 160, 24);
		frame.getContentPane().add(textPenerbit);
		textPenerbit.setColumns(10);
		
		textPengarang = new JTextField();
		textPengarang.setBounds(153, 124, 160, 24);
		frame.getContentPane().add(textPengarang);
		textPengarang.setColumns(10);
		
		textTahunTerbit = new JTextField();
		textTahunTerbit.setBounds(153, 154, 117, 24);
		frame.getContentPane().add(textTahunTerbit);
		textTahunTerbit.setColumns(10);
		
		comboBoxStatus = new JComboBox();
		comboBoxStatus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		comboBoxStatus.setModel(new DefaultComboBoxModel(new String[] {"Tidak Tersedia", "Tersedia"}));
		comboBoxStatus.setBounds(153, 184, 117, 24);
		frame.getContentPane().add(comboBoxStatus);
		
		btnSimpan = new JButton("Simpan");
		btnSimpan.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnSimpan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String judul = textJudulBuku.getText();
				String pengarang = textPenerbit.getText();
				String penerbit = textPengarang.getText();
				int tahunTerbit = Integer.parseInt(textTahunTerbit.getText());
				Object status = comboBoxStatus.getSelectedIndex();
		
				insertData(judul, pengarang, penerbit, tahunTerbit, status);
			}
		});
		
		btnSimpan.setBounds(47, 226, 85, 21);
		frame.getContentPane().add(btnSimpan);
		
		JButton btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editData(textIdBuku.get(table.getSelectedRow()));
			}
		});
		btnEdit.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEdit.setBounds(185, 226, 85, 21);
		frame.getContentPane().add(btnEdit);
		
		JButton btnHapus = new JButton("Hapus");
		btnHapus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int respons = JOptionPane.showConfirmDialog(null, "Apakah Anda yakin ingin menghapus data ini?");
				if(respons==0) {
					if(table.getSelectedRow()>=0) {
						hapusData(textIdBuku.get(table.getSelectedRow()));
					}
				}
				else {
					JOptionPane.showMessageDialog(null, "Hapus data dibatalkan");
				}
			}
		});
		btnHapus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnHapus.setBounds(319, 226, 85, 21);
		frame.getContentPane().add(btnHapus);
		
		JButton btnCetak = new JButton("Cetak Laporan");
		btnCetak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName(JDBC_DRIVER);
					conn = DriverManager.getConnection(DB_URL, USER, PASS);
					String sql = "SELECT * FROM buku";

					JasperDesign jdesign = JRXmlLoader.load("C:\\Users\\user\\eclipse-workspace\\UAS_PBO\\projectuaspbo_12525_13106_12789\\ProjectUASPBO_12525_12789_13106\\res\\ReportDataBuku.jrxml");

					JRDesignQuery updateQuery = new JRDesignQuery();
					updateQuery.setText(sql);
					jdesign.setQuery(updateQuery);
					
					JasperReport Jreport = JasperCompileManager.compileReport(jdesign);
					JasperPrint JasperPrint = JasperFillManager.fillReport(Jreport, null, conn);

					JasperViewer.viewReport(JasperPrint, false);
					
					ExportToExcel exportExc = new ExportToExcel(table, new File("Laporan Data Buku.xls"));
					
					
				} 
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnCetak.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCetak.setBounds(447, 226, 120, 21);
		frame.getContentPane().add(btnCetak);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(47, 269, 520, 148);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int id = Integer.parseInt(textIdBuku.get(table.getSelectedRow()).toString());
				getData(id);
				btnSimpan.setEnabled(false);			}
		});
		table.setFont(new Font("Tahoma", Font.PLAIN, 12));
		scrollPane.setViewportView(table);
	}
	
	public void insertData(String judul, String pengarang_buku, String penerbit_buku, int tahun_terbit_buku, Object status_buku) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			stmt = conn.createStatement();
			
			String q = "INSERT INTO buku (judul_buku, pengarang, penerbit, tahun_terbit, status) VALUES (?, ?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(q);
			
			ps.setString(1, judul);
			ps.setString(2, pengarang_buku);
			ps.setString(3, penerbit_buku);
			ps.setInt(4, tahun_terbit_buku);
			ps.setObject(5, status_buku);
			
			ps.execute();
			
			stmt.close();
			conn.close();	
		}
		catch(Exception e) {
			System.out.println(e);
		}
		showData();
	}
	
	public void showData() {
		model = new DefaultTableModel();
		model.addColumn("Judul Buku");
		model.addColumn("Pengarang");
		model.addColumn("Penerbit");
		model.addColumn("Tahun Terbit");
		model.addColumn("Status");

		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			stmt = conn.createStatement();
			
//			String q = "INSERT INTO buku (judul_buku, pengarang, penerbit, tahun_terbit, status) VALUES (?, ?, ?, ?, ?)";
//			PreparedStatement ps = conn.prepareStatement(q);
//			
//			ps.setString(1, judul_buku);
//			ps.setString(2, pengarang);
//			ps.setString(3, penerbit);
//			ps.setInt(4, tahun_terbit);
//			ps.setInt(5, status);
//			
//			ps.execute();
			
			rs = stmt.executeQuery("SELECT * FROM buku");
			
			textIdBuku = new ArrayList<Integer>();
			while(rs.next()) {
				model.addRow(new Object[] {
						rs.getString("judul_buku"),
						rs.getString("pengarang"),
						rs.getString("penerbit"),
						rs.getString("tahun_terbit"),
						convertStatus(rs.getInt("status"))		});	
						textIdBuku.add(rs.getInt("buku_id"));
			}
			
			
			stmt.close();
			conn.close();
			
			table.setModel(model);
			table.setAutoResizeMode(1);
		} 
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Koneksi Gagal");
		}
	}
	
	public static String convertStatus(int status) {
		if (status == 0) {
			return "Tidak Tersedia";
		}
		else {
			return "Tersedia";
		}
	}
	
	public void editData(int id) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			stmt = conn.createStatement();
			
			String q = "UPDATE buku SET judul_buku=?,pengarang=?, penerbit=?, tahun_terbit=?, status=? WHERE buku_id=?";
			PreparedStatement ps = conn.prepareStatement(q);
			
		
			ps.setString(1, textJudulBuku.getText());
			ps.setString(2, textPenerbit.getText());
			ps.setString(3, textPengarang.getText());
			ps.setInt(4, Integer.parseInt(textTahunTerbit.getText()));
			ps.setObject(5, comboBoxStatus.getSelectedIndex());
			ps.setInt(6, id);
			
			ps.execute();
			
			JOptionPane.showMessageDialog(null, "Data berhasil diupdate");
			
			stmt.close();
			conn.close();	
		}
		catch(Exception e) {
			System.out.println(e);
		}
		showData();
		resetForm();
	}
	
	public void resetForm() {
		textJudulBuku.setText("");
		textPenerbit.setText("");
		textPengarang.setText("");
		textTahunTerbit.setText("");
		comboBoxStatus.setSelectedIndex(0);
		btnSimpan.setEnabled(true);
	}
	
	public void hapusData(int id) {
		try	{
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			stmt = conn.createStatement();
			
			PreparedStatement ps = conn.prepareStatement("DELETE FROM buku WHERE buku_id=?");
			ps.setInt(1,  id);
		
			ps.execute();
			
			stmt.close();
			conn.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		showData();
		resetForm();
	}
	
	public void getData(int id) {
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			
			stmt = conn.createStatement();
			
			//int kode = Integer.parseInt(id);
			
			String q = "SELECT * FROM buku WHERE buku_id=?";
			PreparedStatement ps = conn.prepareStatement(q);
			
			ps.setInt(1, id);
			
			//ps.execute();
			
			rs = ps.executeQuery();
			
			rs.next();
			
			textJudulBuku.setText(rs.getString("judul_buku"));
			textPenerbit.setText(rs.getString("pengarang"));
			textPengarang.setText(rs.getString("penerbit"));
			textTahunTerbit.setText(rs.getString("tahun_terbit"));
			comboBoxStatus.setSelectedItem(convertStatus(rs.getInt("status")));
			
			stmt.close();
			conn.close();	
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}

